{ compiler ? "ghc8104", sources ? import ./nix/sources.nix }:

let
  pkgs = import sources.nixpkgs { };

  gitignore = pkgs.nix-gitignore.gitignoreSourcePure [ ./.gitignore ];

  hpkgs = pkgs.haskell.packages.${compiler}.override {
    overrides = self: super: {
      "REPLACEME" = self.callCabal2nix "REPLACEME" (gitignore ./.) { };
    };
  };

  shell = hpkgs.shellFor {
    packages = p: [ p."REPLACEME" ];
    buildInputs = with pkgs.haskellPackages; [
      (pkgs.haskell-language-server.override {
        supportedGhcVersions = [ "8104" ];
      })
      pkgs.niv
      cabal-install
      hlint
      floskell
      implicit-hie
      cabal-fmt
    ];
  };

in {
  inherit shell;
  "REPLACEME" = hpkgs."REPLACEME";
}
